package com.galvanize;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
public class LightSaberTest {
    @Test
    public void testSetCharge() {
        //setup
        LightSaber ls = new LightSaber(1234);
        //execution
        ls.setCharge(100);
        //assertions
        assertEquals(100, ls.getCharge());
    }

    @Test
    public void testGetCharge() {
        //setup
        LightSaber ls = new LightSaber(1234);
        //execution
        ls.setCharge(50);
        //assertions
        assertEquals(50, ls.getCharge());
    }

    @Test
    public void testSetColor() {
        //setup
        LightSaber ls = new LightSaber(1234);
        //execution
        ls.setColor("blue");
        //assertions
        assertEquals("blue", ls.getColor());
    }

    @Test
    public void testGetColor() {
        //setup
        LightSaber ls = new LightSaber(1234);
        //execution
        ls.setColor("purple");
        //assertions
        assertEquals("purple", ls.getColor());
    }

    @Test
    public void testGetJediSerialNumber() {
        //setup
        LightSaber ls = new LightSaber(12377);
        //execution
        long serialNumber = ls.getJediSerialNumber();
        //assertions
        assertEquals(12377, serialNumber);
    }

    @Test
    public void testUse() {
        //setup
        LightSaber ls = new LightSaber(1234);
        //execution
        ls.setCharge(100);
        ls.use(10);
        float actualCharge = 100;
        actualCharge -= (10.0f / 60.0f) * 10.0f;
        //assertions
        assertEquals(actualCharge, ls.getCharge());
    }

    @Test
    public void testGetMinutesRemaining() {
        //setup
        LightSaber ls = new LightSaber(1234);
        //execution
        ls.setCharge(100);
        float actualRemainingMinutes = (100.0f / 10.0f) * 30;
        //assertions
        assertEquals(actualRemainingMinutes, ls.getRemainingMinutes());
    }

    @Test
    public void testRecharge() {
        //setup
        LightSaber ls = new LightSaber(20);
        //execution
        ls.recharge();
        //assertions
        assertEquals(100, ls.getCharge());
    }





}
